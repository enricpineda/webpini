---
title: "Reimaginant el calendari"
date: 2024-01-16T04:00:00+02:00
draft: false
---
![Full de calendari amb el mes de novembre](/calendari.jpg)

Un calendari és aquella eina que ens hem donat els humans entre tots per saber en quin dia som, per tenir consciència del pas del temps, i com a guia per celebrar coses importants -- naixements, festes, commemoracions, etc. Però també és una cosa arbitrària que varia segons els humans que tenen el poder en aquell moment: si ets jueu, cristià, musulmà, xinès o maia, el teu calendari serà d'una manera o d'una altra, i a més començarà un dia o un altre.

Per això, en un món tan globalitzat, qualsevol decisió de prendre una de les opcions com l'estàndard per comptar dies, mesos i anys pot interpretar-se com una imposició. Qui diu que el calendari gregorià, el calendari fet servir a tot arreu, és el que s'ha de seguir? Per què l'any 1 és l'any del naixement de Jesucrist, i no el de qualsevol altre personatge important? I com és que no hi ha any 0? Per tot això, jo en aquest humil article us porto una proposta de calendari que aglutina dos propostes interessants d'estandarització sota el nom de **Holocena Fiksa Kalendaro**.

## Comptem anys amb l'era Holocena

L'holocè és aquella era que comença, per consens internacional, l'any 10.000 a.C. El moment en què els humans vam deixar de ser caçadors - recolectors i vam esdevenir sedentaris, agricultors, ramaders i, en definitiva, humans. Aquesta proposta no és pas nova: [Cesare Emiliani](https://ca.wikipedia.org/wiki/Cesare_Emiliani), un paleooceanògraf italo-americà, va proposar que el còmput dels anys comencés en aquesta època, de manera que no hi hagués gairebé esdeveniments lligats a la cultura humana anteriors a l'any 0. 

I com es calcula això? Doncs és ben fàcil: si l'any és després de Crist, s'afegeix 10.000 al número de l'any. Si és anterior, es resta aquest any de 10.001. Així doncs, la pandèmia del Coronavirus va passar l'any 12020, l'inici de la I Guerra Mundial l'any 11914, i la Gran Piràmide de Quèops l'any 7441. Nosaltres, ara mateix, en el moment en què escric aquest article, estem a l'any 12024.

## Dies i mesos segons el Calendari Fix

Arribats al punt de comptar dies i mesos, ens trobem que el calendari gregorià té 12 mesos, amb un nombre variable de dies -- uns en tenen 30, un altres 31 i un té 28 o 29, segons l'any. A més, una mateixa data, depenent de l'any, cau en un dia de la setmana o en un altre. Tot un embolic!

Doncs per solucionar això hi ha la [proposta de Cotsworth](https://ca.wikipedia.org/wiki/Calendari_fix_internacional). Aquest senyor va dividir els dies de l'any en 13 mesos de 28 dies cadascun. Cada mes té 4 setmanes justes, sempre comença en diumenge i sempre acaba en dissabte. I si ets lleuger de ment, hauràs pogut veure que tretze per vint-i-vuit fan tres-cents seixanta-quatre dies, de manera que ens en falta un. Aquest darrer dia, el de Final d'Any, s'afegeix al final del calendari, però no cau en cap dia de la setmana ni en cap mes, és un dia "especial". I si l'any és de traspàs? Cap problema! el dia que ens sobra es posa entre juny i juliol, i tampoc compta ni com a dia de la setmana ni del mes, és el Dia del Traspàs.

*Actualització a 16 d'agost de 2024 (CG)*: Respecte aquesta proposta, jo he fet una variant: segint la ISO 8601, que establleix les normes per a representar les dates a nivell internacional, el primer dia de la setmana més comú és el dilluns, de manera que, en la meva proposta, em desmarco de Cotsworth i la setmana, els mesos i l'any comencen en dilluns.

## Holocena Fiksa Kalendaro

Finalment, tenim un any amb 13 mesos, completament regular (excepte els dos dies especials que hem dit) que sempre comença en dilluns i sempre acaba en diumenge, i que a més està basat en un fet comú a tota la humanitat. Ja només falta posar-li un nom en una [llengua internacional àmpliament acceptada com la més desenvolupada de totes](https://ca.wikipedia.org/wiki/Esperanto) i ja ho tindríem.

Si teniu curiositat per veure quin format tindria qualsevol data que us vingui al cap, [podeu consultar un JSFiddle que vaig crear](https://jsfiddle.net/enricpineda/joa8L32p/13/) amb un conversor molt senzillet. A banda d'això, si em seguiu per Mastodon us donaré el bon dia amb les dues dates, la gregoriana i la del HFK. Les mateixes dates que trobareu a l'encapçalament d'aquest mateix post del blog.

I recordeu que en podem parlar de tot plegat a les xarxes socials (Mastodon, Instagram) o per correu electrònic.