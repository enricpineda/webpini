---
title: Darwinisme digital
date: 2024-07-09T00:15:00+02:00
draft: false
---

![Noia mirant preocupada la pantalla d'un portàtil](/noia-portatil.webp)

Començaré fent un aclariment: quan parlo de darwinisme no ho faig igual que aquells que es van inventar [els premis Darwin](https://darwinawards.com/), on premiaven a la gent que, amb la seva mort estúpida o la seva manera idiota de quedar-se estèrils, ajuden a purgar l'ADN humà d'imbècils. No *amichs*, jo em refereixo a la capacitat que sembla tenir força gent per adaptar-se a un medi encara que aquest provi de fer difícil la seva supervivència. O la seva comoditat, no cal ser tan tragicòmics.

Us poso en situació: porto uns dies provant Threads, la xarxa de microblogging de Meta que pretén competir amb X i Bluesky (encara que aquest darrer sembla que ha nascut mort), i que en un futur podria arribar a federar amb serveis com Mastodon, Lemmy, Pleroma i similars del Fedivers. Deixant de banda aspectes que fan fàcil la seva entrada, com pot ser tenir un compte previ d'Instagram, els problemes per entrar son els mateixos que el Fedivers: és possible que si vens de X no trobis la mateixa gent, el funcionament és similar però no igual, i fa mandra instal·lar noves aplicacions al telèfon, la veritat. Així doncs, a priori, hi ha els mateixos recels per mudar-s'hi que amb el Fedivers, mirant-ho des del punt de vista d'un usuari de X, però l'ambient és millor (no molt, però millor) que el que un troba a Can Elon.

I aquí és on vull arribar: si ja fas servir una plataforma com X, i portes temps aguantant els canvis al *timeline*, la publicitat, els intents de fer-ho de pagament, l'ambient general de crispació permanenti la degradació en el servei de forma constant, potser has evolucionat i t'has convertit en aquells organismes que, malgrat les condicions ambientals que farien que cap ésser viu s'hi volgués acostar, sobreviuen i s'adapren i potser troben la manera de reproduir-se i tot. I és clar, ara fer l'esforç per readaptar-se i aclimatar-se a nous ambients, per molt més sans i agradables que siguin, costa molt.

Així doncs, almenys aquest estiu em repensaré allò de fer apologia del Fedivers entre ela usuaris de X, Threads o Instagram. Jo aniré voltant i saltant entre ambients, protegit de forma convenient, tot sabent que casa meva és el servidoret on tinc el Mastodon muntat i la comunitat segura creada. I si algú més vol venir, benvingut serà. però no penso fer gaires esforços més per augmentar la massa social del Fedivers.