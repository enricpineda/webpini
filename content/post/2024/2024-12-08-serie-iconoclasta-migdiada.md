---
title: "Una sèrie iconoclasta per fer la migdiada"
date: 2024-12-08T00:00:00+02:00
draft: false
---

![Els protagonistes de la sèrie](/comsifosahir.webp)

L'any 1994 serà recordat entre els espectadors de TV3 com l'any en què s'estrenava una de les telenovel·les catalanes de més bon record: "Poble Nou". Aquella sèrie seria el tret de sortida cap a un nou fenòmen català, la sèrie de la tarda, que de forma molt valenta va ser pionera i va trencar moltíssims tabús en horari de màxima audiència. Personatges gais amb petons en primer pla, trames que combinaven incestos i fills ilegítims en un paisatge que volia ser el nostre Falcon Crest, o fins i tot senyores grans que, un cop divorciades, tornaven a engegar la seva vida. Tot tractat amb respecte, però ocupant planes i planes de debats als mitjans sobre com n'éren de trencadores aquelles sèries.

Tanmateix, personalment no he trobat en la història de la "teletrès" una sèrie que al llarg de 8 temporades hagi trencat tants i tants esquemes com una innocent sèrie de migdia anomenada "Com si fos ahir". Aquesta sèrie que tracta de les vides d'un grup d'amics d'institut que es retroben després d'assistir a l'enterrament d'un amic en comú (com a trama d'un capítol inicial té poc de convencional) s'ha anat desenvolupant d'una manera que per mi és genial, combinant **situacions normals i quotidianes en primer pla amb fets absolutament trencadors o directament surrealistes en segon pla**. I si no, vegem-ne un quants exemples:

1) Una parella de gent gran es trenca perquè ell s'ha enamorat d'uns altra persona, i ella es refà amb una altra parella. Només que la parella d'ell és la vídua del seu propi fill i la parella d'ella és una altra dona. Però el tema principal és com ho entoma el seu fill de 50 anys.

2) Els personatges es relacionen afectivament entre ells i tenen problemes, problemes que passen a primer pla llevat que una relació d'aquestes és una "triella", una altra és amb un noi trans (que crec que fa el primer nu parcial d'un actor trans a la història de TV3) i una altra és una relació de "cardamics" entre dos persones de més de 85 anys.

3) Apareixen conceptes com el ciberasatjament, les estafes piramidals, les criptomonedes, la precarietat laboral, les addiccions o la mort, tots tractats amb naturalitat enmig de trames curtíssimes que s'acaben en un o dos capítols i que, almenys en el meu cas, fan que se'm quedin molt millor a dins del cap que no pas amb trames llarguíssimes, trames que deixen per episodis hilarants com **unes presències fantasmals en una casa**.

I és per tot això que, tot i no seguir-la amb regularitat, sempre que puc em quedo mirant-la i segueixo les trames que s'hi presenten. Perquè crec que l'equip de guionistes han sabut muntar unes trams atractives, i els actors un personatges tan ben fets, que a partir d'ara et poden vendre el que vulguin i els espectadors de la sèrie les compraran igual. Total, ja han aconseguit fins i tot posar com a subtrama d'una història de gelos una telesèrie a dins de la telesèrie, com si fos "Inception".

I què voleu que us digui, si una de les guinistes és la **Maria Jaén**, a mi ja em tenen comprat. Bé, a mi i a molts homes de més de 45 anys que recordem encara aquell "Amorrada al piló" 😉.