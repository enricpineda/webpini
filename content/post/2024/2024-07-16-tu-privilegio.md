---
title: Tu privilegio
date: 2024-07-16T20:00:00+02:00
draft: false
---

![Grupet de nanos joves parlant davant d'un ordinador](/priviegi.webp)

*Com a fet extraordinari faré aquest article en castellà, perquè em sento amb la necessitat d'explicar a tots aquells que són monolingües a Espanya (i que per tant ho són en castellà) les raons per les quals han d'afegir un nou privilegi al llistat que segurament ja tenen*.

Últimamente me encuentro enganchado a la red social Threads, el nuevo invento del creador de Meta que pretende hacerle la competencia a la X de Elon Musk. En esta red social, el algoritmo ha decidido, seguramente guiado por mi ubicación, que me interesan posts de gente que vive en España, y el hecho de que me gusten los idiomas, un hecho que a lo mejor ha recabado de mi actividad en Whatsapp o Instagram, hace que mi línea temporal principal esté llena de gente hablando en castellano hablando del castellano en sí.

Y leyéndolos con calma veo en todos un patrón: orgullo por la lengua de Cervantes, estupefacción por el hecho de tener que dirigirse a turistas y extranjeros en inglés estando en España, y en general un sentimiento de reafirmación del castellano, o español, como uno de los pilares del Reino de España, si no de la humanidad. Desde el punto de vista de un catalán, que es ciudadano español pero que tiene una lengua materna diferente al castellano, no deja de ser chocante y significativo, sobretodo cuando se trata de reafirmar una idea que llevo barajando desde hace unos días.

Me dirijo a ti, ciudadano español de izquierdas, votante del ala izquierdosa del PSOE, Podemos o Sumar. No me dirijo a la mayoría de votantes del PSOE, del PP o de VOX / Alvises varios, a los que diga lo que diga siempre encontrarán algún argumento para descalificar lo que propongo aquí, yasea a base de argumentos delirantes o directamente insultos. Amiga izquierdosa, debes añadir un nuevo privilegio al listado que seguramente ya tienes: un privilegio lingüístico.

El privilegio que cualquier ciudadano estadounidense, británico o neozelandés tiene a la hora de viajar por el mundo, sabiendo que automáticamente todos le va a entender, o lo van a intentar, porque todo el mundo ha adoptado su lengua materna, el inglés, como lengua franca mundial, también lo tiene el típico español monolíngüe cuando viaja a una comunidad autónoma en la que hay una lengua cooficial diferente a la suya. Su nivel de estrés es cero a la hora de saber si le van a entender, porque todos y cada uno de los ciudadanos de estas comunidades autónomas (entre los que me incluyo) estamos obligados **por ley** a conocer su lengua, en concreto a tener un conocimiento que permita su comprensión. Y no lo digo yo, [**lo dice la Constitución Española en su artículo 3**](https://www.boe.es/buscar/act.php?id=BOE-A-1978-31229#a3).

Y me dirijo a ti, izquierdosa, porque sé que en el fondo podrás empatizar. Mi bilingüismo no es un don, un tesoro que hay que cuidar y respetar. **Mi bilingüismo es forzado**, y en ningún caso es recíproco. Y si te cuesta creerlo, ponte en mi situación: recuerda la de horas que echaste para aprender inglés y defenderte en la lengua que un ciudadano de Manchester o Chicago tiene como lengua materna. Ellos ten han forzado a ser bilingüe o a intentarlo, igual que la Constitución Española me obliga a mí.

Así pues, añade este privilegio al listado que seguramente ya tienes, y que seguramente estás intentando superar. Entre todas podemos conseguir que, poco a poco, la justícia lingüística pueda llegar a ser una realidad.