---
title: "Permete'm que ho apunti"
date: 2024-10-06T00:00:00+02:00
draft: false
---

![Persona escrivint en un bloc de notes amb una ploma](/blocdenotes.webp)

Posem-nos en situació: **sóc una persona que oblida coses**. I amb els anys, sembla que la cosa va a més. Costa molt tenir el mateix nivell d'atenció en absolutament tots els àmbits de la meva vida, i no entendre això m'ha portat, molts cops a la frustració de no entendre per què el meu cervell s'entesta en arxivar coses que no cal arxivar. I és clar, la culpa és meva: **el cervell necessita fer lloc** i jo no li deixo a base de bombardejar-lo amb dades, pensaments i idees bona part del dia.

El cas és que des de fa un temps que busco una bona manera de gestionar tot allò que he de tenir controlat mitjançant notes. I com que sóc un d'aquells gairebé *cincuentones* que veu en les noves tecnologies (que ja no són tan noves) la solució a molts dels problemes de la vida quotidiana, he provat un munt de sistemes de gestió de la informació personal. Notes de l'Evernote, llistes de tasques a Google Tasks, solucions integrals com ClickUp... fins i tot els taulells a l'estil Kanban que té integrats el Tasks de Microsoft Office. Qualsevol sistema que em permetés tenir a mà tot el que em calia fer, ja sigui a la feina com en l'àmbit familiar o associatiu s'ha provat. I, per alguna raó que se m'escapava, **cap d'elles m'ha acabat anant bé al cent per cent**.

I la raó m'ha vingut mirant un vídeo a Youtube. Bé, una pila de vídeos a Youtube on una colla de *frikis* asseguraven que amb dos objectes mot senzills, que conjuntament no costen més de 2 euros, han aconseguit posar ordre a la seva vida i a la seva ment d'una tacada. I aquests dos objectes els tenim molt a l'abast de les nostres mans: **una llibreta i un boli**.

Després de veure tot de vídeos en el que persones de molts tipus diferent lloaven les excel·lències de fer servir una llibreta i un boli per anotar tot allò que se'ls passava pel cap al llarg del dia, **he decidit que jo també ho vull provar**, i ara porto amb mi una llibreta i un boli. Tan simple com això. I de moment, després de quatre dies de fer-la servir per apuntar tot de coses que em passen pel cap, he pogut constatar això:

1) **El cap no em va tant a 100 per hora**: el fet d'anotar en un lloc allò que vull recordar, o que se m'ha passat pel cap, fa que el meu cervell ho tregui de la memòria a curt termini, i eviti anar-ho repetint en bucle al meu cap durant tot el dia. Anoto el que vull tenir present, ho etiqueto mínimament, i tanco la llibreta. I amb aquest gest, tanco el cap i el puc dedicar a coses una mica més productives.
2) **No em distrec**: el pal de fer servir un sistema basat en una eina informàtica, ja sigui a l'ordinador com al telèfon, fa que sense voler estiguem més exposats a rebre notificacions que fan que, en última instància, ens puguem distreure del nostre objectiu d'apuntar quelcom perquè resulta que hem de veure el Mastodon o la missatgeria d'Instagram. **Més temps amb la llibreta és menys temps amb el mòbil**, i per tant menys distraccions. Així de senzill.
3) **Apuntar em port a fer altres coses**: voler apuntar idees, cites o pensaments a la llibreta és un alicient per reprendre un hàbit tan necessari com el de la lectura. Ara, em dedico a llegir llibres amb la llibreta al costat, i apunto a la llibreta allò que m'ha fet pensar, riure, estremir, etc. De cop i volta, **allò que llegeixo es queda més al meu cap**, ja se sap que apuntar fa que els conceptes es retinguin millor al dins nostre.
4) **La meva lletra a millorat (una miqueta)**: amb això d'anar apuntant  coses, i com que al final qui s'ho  ha de llegir és un mateix, hom podria caure en el parany de fer mala lletra per anar ràpid. Doncs aneu equivocats: seure, mirar de fer bona lletra i anar a poc a poc fa que un pensi el que escriu, gestioni millor la informació, i al final aprofiti millor allò que ha escrit a la seva llibreta.

Així doncs, si encara no heu provat això d'apuntar-vos no només les coses que heu de fer al llarg del dia en una llibreta, sinó també els vostres pensaments, idees o anades d'olla, no sabeu el que us perdeu. Aneu ja al basar a agafar la primera llibreta que us agradi i un bolígraf, i a apuntar! Ah, i com a idea final: si ets un fan del concepte de decreixement, creus que hi ha una cosa més "decrecentista" que un bloc de notes fet amb paper reciclat per tu mateix?

Ja sabeu, podeu seguir amb mi el debat a Mastodon, a Instagram o per carta, si ho voleu.