---
title: Què votar aquest 12M - els partits independentistes
date: 2024-05-07T00:15:00+02:00
draft: true
---

# Què votar aquest 12-M: els partits independentistes

Un cop més ja tenim aquí les eleccions al Parlament de Catalunya, i un cop més tenim aquí una anàlisi dels programes electorals dels partits que s'hi presenten, anàlisi que ningú ha demanat, però que si ajuda a algú a decidir el seu vot de cara a les eleccions, doncs millor.

En aquest cas, analitzarem els programes electorals dels partits no independentistes, o que no tenen una posició clara al respecte.

## Ciudadanos
El partit taronja afronta les eleccions amb un objectiu clar: aconseguir representació. Lluny estan els resultats guanyadors del 2017, o els lideratges d'Albert Rivera i Inés Arrimadas. El programa, que defensa una bilingüització efectiva de Catalunya [amb un programa que només es pot llegir en castellà](https://www.ciudadanos-cs.org/var/public/documentos/2024-05/programa-electoral-12m.pdf), també defensa perles com la construcció de la B-40, l'ampliació de l'Aeroport del Prat i la defensa de l'escola concertada i privada. Paradoxalment, també defensen millors serveis públics amb una baixada general d'impostos.

**Els votaràs si**
* Et vas quedar, electoralment, a l'any 2010
* No t'espanta un neoliberalisne bastant potent
* Et cau bé Jordi Cañas, cap de campanya de Carrizosa

**La proposta estrella**

Els funcionaris públics estaran obligats a informar de conductes contràries a la Constitució Espanyola. Policia política al servei de la Nación.
## Comuns - Sumar
Sí, aquesta gent eren els de Podem, i abans els d'Iniciativa per Catalunya. La candidatura de gent tan dispar com Jaume Asens i Ernest Urtasun. I sí, la candidatura de la Colau. I si estan aquí és perquè defensen un referèndum amb unes condicions tan impossibles per a la seva realització que és pràcticament una quimera. Això sí, en quant a polítiques socials, [són dels més complets, progressistes i sensats d'aquesta banda](https://img.beteve.cat/wp-content/uploads/2024/04/programa-comuns-sumar-eleccions-catalunya-2024-270424.pdf).

**Els votaràas si**
* Vas votar a la Colau
* Ets d'aquells que es consideren "ciutadans del món"
* Vols una Presidenta de la Generalitat enmig de tants naps

**La proposta estrella**

Tota una secció completa dedicada al **programari lliure**, i al seu ús a dins de l'Administració Catalana.

## Convergents

No faig conya. Els hereus de la política del peix al cove d'en Pujol o dels "trapis" d'en Prenafeta encara existeixen, reclamen el seu espai, i es van presentant a les eleccions sempre que poden. Tradició catalana, liberalisme econòmic i suport a la pagesia [en un programa que et recordarà els vells temps d'ordre](https://www.convergents.org/2024/04/18/programa-electoral-per-a-les-eleccions-al-parlament-de-catalunya/).

**Els votaràs si**
* Vius a Tarragona (només es poden presentar allà)
* Ets un pagès que estima Catalunya i en Pujol a parts iguals
* Consideres que en Puigdemont és un perillós eixalabrat

**La proposta estrella**

Amnistia i bon govern. Sí, és el més destacat
## Frente Obrero
Què tal una mica de populisme amb un bon rajolí d'islamofòbia i quatre proclames comunistes? [Doncs això és el Frente Obrero](https://frenteobrero.es/nos-presentamos-a-las-elecciones-catalanas/), un partit que es centra en aturar el que ells consideren com "imparable islamización de España". Són molt unionistes, estan en contra de la ideologia de gènere... Uns perles, vaja!

**Els votaràs si**
* La paraula Islam et provoca urticària, però no ets de dretes com per votar a VOX
* La paraula nacionalsocialisme no et recorda episodis foscos de la història mundial

**La proposta estrella**

Difícil de dir, ja que no tenen programa electoral per aquestes eleccions.
## Izquierda por España

Quan un grup de desencantats amb el PSC-PSOE s'ajunten i decideixen muntar un partit, surten coses com Izquierda en Positivo: socialdemòcrates recentralitzadsrs. Després s'ajunten amb la gent de Democracia Efectiva i Unidos por la Solidaridad i pareixen aquesta coalició electoral, que per no tenir no té ni web ni programa electoral. Per tant, [agafarem el programa del 2021](https://izquierdaenpositivo.es/2021/02/01/programa-electoral-de-izquierda-en-positivo-para-las-elecciones-al-parlament-de-catalunya-2021/).

**Els votaràs si**
* Votaries al Pedro Sánchez però et fas por que pacti amb els indepes
* Vius a Barcelona o Tarragona
* T'agradaria poder votar totes les propostes per Internet i no coneixes a Pirates de Catalunya

**La proposta estrella**

Incrementar la presència de l'Estat a Catalunya en tant que "divulgador dels valors democràtics que comparteixen tots els espanyols" (sic).
## PACMA

El partit extraparlamentari més votat a Espanya torna a unes catalanes després de no obtenir els avals l'any 2021. [El programa és clar](https://pacma.es/wp-content/uploads/2024/05/PE_CATALUNYA_12M24.pdf), i es centra en la protecció dels animals i de la flora del territori. Per la resta de propostes electorals, tenim molts punts en comú amb els partits tirant a progressistes.

**Els votaràs sí**
* Vols votar a un partit ecologista de veritat
* No t'importa que probablement no treguin cap representant
* Literalment, tens gos i pateixes quan fan el castell de focs del teu poble

**La proposta estrella**

La prohibició de fer servir animals en espectacles tradicions i festejos, prohibint-ne també la seva difusió a través dels mitjans.
## Partit Comunista dels Treballadors de Catalunya

Comunistes de veritat, sense pèls a la llengua. [Un programa polític](https://pcte.es/wp-content/uploads/2024/04/PROGRAMA_DE_LUCHA_CATALANAS_2024.pdf) marcat per la nacionalització de la producció, l'estatalització de la vida pública i el poder obrer. Una mica com tornar a la idea marxista-leninista, però una mica modernitzada.

**Els votaràs si**
* Ets un comunista convençut que considera als Comuns una colla de venuts al capital
* Estàs per la República Obrera Mundial
* No et fa res que el [logo del partit tingui una falta garrafal](https://s3.eu-west-3.amazonaws.com/cdn.pcte.es/wp-content/uploads/2021/01/24181233/LOGO_PCTC_RGB-26.svg).

**La proposta estrella**

Exigir la fi de la complicitat de la Generalitat amb el règim sionista
a Israel. Amb la que està caient són els únics qwue es pronuncien sobre això.
## Partido Popular

El PP torna a la càrrega disposat a recuperar els votants que van marxar a Ciutadans i que encara no s'han passat a Vox. El seu candidat, Alejandro Fernández, és molt conegut a Tarragona, on es va convertir en alcalde [gràcies al seu jingle electoral, que es va tornar viral](https://youtu.be/zi9lPWx72rU?si=XTYFl7WuTzt9rITf). 

**Els votaràs sí**
* Ets espanyolista, d'ordre, i et molesta que el teu discurs xenòfob i anticatalanista se l'hagi apropiat VOX
* Et creus allò de la millora dels serveis públics amb una baixada genreal d'impostos.
* No et fa res que el [programa electoral sigui un fulletó de 12 punts](https://www.ppcatalunya.com/wp-content/uploads/2024_pag_home/Flyer_BARCELONA.pdf).

**La proposta estrella**

Difícil de dir quan només tens 12 punts al programa.
## PSC-PSOE

Desenganyem-nos, és l'únic d'aquest bloc amb possibilitats per governar. Illa ha muntat un govern alternatiu que s'ha treballat un [programa electoral enorme](https://www.socialistes.cat/wp-content/uploads/2024/04/Programa-electoral-PSC-Eleccions-12-maig-2024.pdf) que costa molt de llegir. Tanmateix, estem en les mateixes: si governa ell, governa Sánchez en l'ombra.

**Els votaràs si**
* Ets del Vallès i a casa sempre heu votat socialista
* Encara veus possible una reedició del Tripartit o, en el seu defecte, un bipartit amb els Comuns
* Ets d'esquerres però sense passar-se

**La proposta estrella**

En aquest cas, la proposta estrella és tenir el [programa en format de Lectura Fàcil](https://www.socialistes.cat/wp-content/uploads/2024/05/Programa-electoral-PSC-lectura-facil.pdf), cosa que crec que haurien de tenir **tots els partits**.
## Per Un Món Més Just

Buscaves un partit amb innocència i *happyflowerisme*? Doncs ja el tens aquí. Liderat per un advocat de 78 anys, es presenten com l'alternativa solidària i justra amb tothom, posant un accent molt significatiu en el col·lectiu inmigrant. Vaja, una antítesi de VOX, FO, Aliança, etc.

**Els votaràs si**
* Creus que la solidaritat ho resol tot
* Confies molt en el gènere humà
* El seu candidat et provoca tendresa.

**La proposta estrella**

és l'únic partit que he vist que es solidaritza amb els 1078 represaliats per l'u d'octubre, **i amb els 350 policies nacionals** ferits durant aquells dies. Això sí que és ser solidari i just!
## Recortes Cero

El partit hereu de la Unificación Comunista de España, l'únic partit espanyol en ser considerat per alguns experts com una secta, es torna a presentar amb un programa electoral [bàsic i molt general](https://www.lahaine.org/est_espanol.php/uce-secta-de-zumbados-que-se-hacen-pasar): no a les retalllades, nacionalització de la banca i resdistribució de la riquesa, i unitat nacional 

**Els votaràs si**
* Alguina vegada els has comprat el diari "De verdad" que venen per les entrades del metro a Barcelona
* Havies estat afiliat a la UCE
* Ets un jubilat al qual t'han entabanat dient que lluitaran contra les retallades.

**La proposta estrella**

Si t'has mirat el programa, veuràs que està com "en construcció" des del 2021. O sia, que no esperis gaires propostes estrella perquè no les tenen.
## VOX

Amb un lema completament bèl·lic i un cartell electoral on Santiago Abascal suert més gran que el propi Ignacio Garriga, VOX es presenta amb [un programa de 50 pàgines](https://drive.google.com/file/d/11XFVfyJ6ZGluHPVWXOPOfmQFOgFK6pgh/view) trufat de referències a la inmigració ilegal, al separatisme i a la defensa dels valors tradicionals espanyols, tot amanit amb negacionisme climàtic, masclisme i demés perles. Programa que, òbviament, només pots llegir en castellà malgrat la seva suposada defensa de la llengua catalana.

**Els votaràs si**
* Ets un enyoradís de la dictadura de Franco
* Veus un niqab i automàticament penses en bombes
* Consideres que Catalunya és molt espanyola, "y mucho española"

**La proposta estrella**

Reintroduir la tauromàquia a tot arreu, i promocionar-la a nivell turístic com un reclam. La cultura de la mort, vaja.

I fins aquí l'anàlisi dels programes electorals del bloc no independentista. Si vols conèixer l'anàlisi que faig dels partits del bloc *indepe*, fes click en aquest enllaç.


