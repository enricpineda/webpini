---
title: "Posa el teu domini a Bluesky"
date: 2024-12-01T00:00:00+02:00
draft: false
---

![Un mòbil amb el logo de Bluesky](/bluesky.webp)

Probablement estiguis llegint això perquè ets de les persones que ha decidit marxar de Twitter / X per la raó que sigui. I segurament, gràcies al fet que **Bluesky és una còpia més o menys acurada del que era Twitter fa uns anys**, hi has anat a parar i comences a fer-lo servir. Doncs bé, celebro que hagis volgut abandonar el cau de feixisme, i tot i que segueixo esperant que algun dia vinguis a veure'ns al Fedivers, de moment et diré que et vagis posant còmode a la xarxa de la papallona, tot i que tampoc gaire.

Ara bé, hauràs vist que el teu nom d'usuari en aquesta xarxa (allò que ells anomenen *handle*) és una mica complicat de recordar, ja que no només és el teu nom d'usuari sinó també el domini "bsky.social", de manera que més que un nom d'usuari **és com un subdomini del domini principal**. I si ets una empresa o una institució una mica preocupada per la teva imatge de marca, has de saber que hi ha una manera més elegant d'estar a la nova xarxa de Jack Dorsey. I és canviant el teu nom d'usuari pel teu domini a Internet. I en aquest petit manualet t'ensenyaré com fer-ho.

## Escollint el teu domini a Internet

Aquest manual dóna per suposat que tens dues coses, a saber:

* Un compte a BLuesky
* Un domini a Internet.

Que no tinguis el primer em faria sospitar una mica, però el segon és ben fàcil d'aconseguir. Per uns 7€ el primer any **pots tenir un domini ".cat"** amb el teu nom en un temps rècord, i si a més el fas en nom meu tu pots tenir un descompte a CDMon (una empresa ben catalana) i jo també tindré algun benefici. Així que anima't i [registra el teu domini a CDMon amb el codi **e844ecabd9d7**](https://www.cdmon.com/ca/dominis/registrar-dominis).

## Activant el teu domini a Bluesky

Un cop ja tens el teu domini necessites dues coses: accés al panell d'administrador del mateix per canviar les entrades del servidor DNS (a CDMon es diuen "registres de zona" però pot ser que al teu proveïdor tinguin un altre nom), i un PC amb un navegador. Si tens accés a aquuest administrador, obre una pestanya del navegador del teu ordinador i entra a l'administrador.

Tot seguit, en una altra pestanya del navegador ves a Bluesky, entra a la secció de "Configuració" i, quan estiguis en aquesta pantalla, ves a la opció "Identificador". Veuràs que s'obre una finestra on et permet canviar el teu nom d'usuari, però just a sota hi ha un botó que posa "Tinc el meu propi domini". Fes click en aquest botó.

Un cop hagis fet click, la finestra canvia i et mostra tres elements:

1) Un camp on et deixa posar el teu domini. Tan se val si li poses les www a l'inici o no, però jo no les he posades perquè queda més guai.
2) Un parell de botons on et pregunta si tens o no accés al panell de control de les DNS. Com que nosaltres en principi en tenim, seguirem les instruccions i crearem un registre de tipus TXT, que tindrà com a subdomini "_atproto.elteudomini.cat", on "elteudomini.cat" és el domini que hauràs posat abans al camp de text anterior, i un valor que hauràs de copiar i enganxar tal qual al registre.
3) Finalment, un cop ho hagis fet, i depenent de la velocitat de refrescament del teu proveïdor de domini, hauràs d'esperar una mica abans d'apretar el botó blau marcat com a "Verifica els registres de DNS" i que et surti un missatge verd de confirmació.

I ja estarà, un cop fet això, el teu nom d'usuari a Bluesky serà el teu propi domini a Internet. I així la teva identitat digital quedarà unificada!

## Consideracions finals

D'una banda, Bluesky és un sistema que, en teoria, federa però ho fa amb un protocol propi. Això vol dir que si estàs prou animat i vols provar-ho, semre pots provar de [muntar-te el teu propi servidor de Bluesky](https://github.com/bluesky-social/pds?tab=readme-ov-file#preparation-for-self-hosting-pds) i poder interactuar amb la resta d'usuari de la xarxa de la papallona. Però vaja, que això no és res que no fes ja Mastodon i la resta d'aplicacions del Fediverse a través del protocol ActivityPub.

Ens veiem a la xarxa de la papallona blava?