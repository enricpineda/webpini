---
title: "Ven els teus llibres vells amb Hamelyn"
date: 2024-10-19T00:00:00+02:00
draft: false
---

![Llibres vells amuntegats](/llibres.webp)

Jo no sé si tu tens el mateix problema que tinc jo, però a casa meva hi ha molts llibres. Massa llibres, fins i tot. I els llibres, tot i ser font de cultura i de coneixement, tenen l'inconvenient que ocupen molt de lloc. I encara que hi ha llibres que mereixen ocupar un lloc per diverses circumstàncies, hem d'entendre en algun moment de la nostra vida, que bona part d'ells els tenim morts de fàstic i ni tan sols els toquem.

Ara bé, què n'hem de fer? No els llençarem a no ser que estiguin molt malmesos. Les donacions a la biblioteca darrerament no són molt viables, ja que solen estar renyides amb les polítiques de fons de les mateixes biblioteques (sí, les bibliotreques compren els llibres seguint uns criteris, encara que sembli que no). Hi ha solucions més imaginatives, com els llibres lliures que circulen per les nostres ciutats o fer paradetes de llibres per regalar per Sant Jordi, però o bé són força laborioses, o bé estan lligades a un dia en concret. És per això que iniciatives com **Hamelyn** calen en un món com el nostre.

Hamelyn et compra els llibres que ja no llegiràs més, te'ls ve a buscar a casa, i tot des de la comoditat de fer-ho des del sofà amb una aplicació. Aquesta aplicació et permet escanejar o introduir manualment els ISBN dels llibres que tinguis, els consulta amb la seva pròpia base de dades, i et retorna un veredicte. Si el llibre té poca demanda, malauradament no l'acceptaran, però **si té demanda us donaran uns cèntims per aquell llibre**. Un cop hagueu escanejat tots els llibres, registreu una adreça postal, un mètode de cobrament (compte bancari o Paypal) i un dia per fer l'entrega. Aquell dia, un missatger vindrà i recollirà els llibres a casa per dur-los a Hamelyn, on valoraran que els llibres estiguin bé i us ingressaran els dinerets.

He fet l'experiment d'escanejar un llistat de llibres concret que vull treure de casa amb aquesta aplicació i amb Momox, una aplicació similar que et permet fer exactament el mateix. Diferències d'usabilitat a banda, m'he trobat que per un número similar de llibres acceptats, Hamelyn ofereix de mitjana més diners per llibre. Així doncs, per mi l'elecció de l'aplicació no ha estat gaire difícil.

Si tens a casa un munrt de llibres que no et penses llegir, i vols treure'n un petit retorn a la inversió que vas fer en el seu moment, potser Hamelyn és una bona solució, tot i que potser no és l'única. Coneixes altres mètodes per vendre llibres usats a bon preu? Passa-me'ls per correu o per xarxes, i aniré actualitzant aquest post amb les solucions que vagi rebent. I fins llavors, aniré fent lloc a la prestatgeria de casa com si no hi hagués un demà.