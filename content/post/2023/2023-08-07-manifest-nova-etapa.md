---
title: "Manifest d'aquesta nova etapa"
date: 2023-08-07T16:00:00+02:00
draft: false
---
Ja feia temps que tenia una idea al meu cap, que consistia en renovar la meva web personal i fer-la el més senzilla possible. Tan senzilla que fos com tornar als orígens, amb una pàgina senzilla, que demanés el mínim i no requerís que els usuaris de la mateixa hagin d'acceptar polítiques de privacitat o donar el seu consentiment a unes cookies que sembla que tothom ha de posar sí o sí per saber quanta gent visita la seva pàgina. Amb això al cap, he fet el següent.

## He creat el web amb Hugo

Una de les coses de les que em volia despendre era de Wordpress. No perquè consideri que és una plataforma que no s'ajusta al que jo voldria, o perquè no m'agradi com és, sinó pel fet que considero que, pel que faig i pel volum d'actualitzacions de la web, és massa complex i gran i farragós. [Hugo](https://gohugo.io), d'altra banda, és un preprocessador de pàgines web que, a través de Markdown i en molts pocs segons, em permet tenir un web actiu i molt ràpid en un obrir i tancar d'ulls. D'acord que encara he de treballar una mica el tema de les plantilles i de com es veu la web a nivell gràfic en general, però intueixo que és un sistema intuitiu d'aprendre, i que em permet tenir un blog creat en local, guardat a Codeberg com a backup, i publicable de forma gairebé instantània al servidor on ho vull allotjar -- el mateix Codeberg.

## L'he allotjat a Codeberg

Codeberg és una alternativa a Github de codi lliure. Després de l'adquisició d'aquest darrer per part de Microsoft, em vaig plantejar moure els meus repositoris a una plataforma que fos de codi lliure, i de moment Codeberg ha guanyat la partida, més que res perquè tenen un projecte anomenat Forgejo (forja, en Esperanto, i amb falta d'ortografia inclosa) que vindria a ser una mena de Github que et pots muntar al teu propi servidor. Això va fer que m'acabés de decidir i que mogués tot allà. I si a més em permeten tenir una web allotjada amb ells a través de Pages, doncs millor que millor.

I la cirereta del pastís: després de plantejar-me el meu propi hosting, he vist que puc vincular el meu domini a Pages de forma fàcil. I gràcies a això, ara ho pots llegir al meu punt cat, i no a qualsevol domini deixat de la mà de Déu.

## No hi ha ni galetes, ni rastrejadors, ni res per l'estil

Com que vull fer una web com es feien abans, i abans no hi havia ni galetes, ni rastrejadors, ni píxels que mirin el que fas i el que no, he decidit que faria el mateix en aquest 2023 i us ofereixo un web senzill sense galetes ni res semblant. No m'importa pas gaire saber qui llegeix què, quan ho fa i des d'on. I de cares als usuaris, visitar un petit raconet de la xarxa lliure d'aquestes tecnologies entenc que també ha de ser diferent, per no dir que ha de ser molt millor. Així que, benvinguda siguis a un espai on no seràs rastrejada -- personeta que visites la meva web. Si em vols dir quelcom, sempre ho pots fer a les meves xarxes socials o al meu correu electrònic.