---
title: "El fenòmen de les tradwives"
date: 2024-08-31T00:00:00+02:00
draft: false
---
![Foto dels anys 50 amb una dona donant al seu marit una mica de menjar per tastar a la cuina](/tradwives.webp)

Són joves, són influenciadores i famoses, i en els seus vídeos promouen un estil de vida basat en el retorn als valors de les dones dels anys 50. Les *tradwives* s'han posat de moda, però darrera d'aquest suposadament innocent *trend* pot ser que hi hagi més coses.

## Què són les *tradwives*

Les *traditional wives* o *tradwives* van néixer com a moviment als Estats Units, i ràpidament van assolir grans quantitats d'atenció, seguidors i visualitzacions de contingut gràcies al fet que oferien quelcom que a priori era innocent: receptes de cuina, consells de neteja i quatre trucs de maquillatge. Adornat amb una estètica pròpia d'una sitcom dels 50 americana, o d'un dels primers capítols de *Wandavision*, ràpidament es va anar tornant una altra cosa: un petit moviment de **noies joves, blanques i cisheteros** que apostaven per un retorn als valors associats a les dones propis dels anys 50 als EUA: **compromís amb la família i les criatures, dependència econòmica del marit, i feminitat extrema.**

Fixeu-vos en això: noies joves fent vídeos per tornar a les essències de la mestressa de casa tradicional, tot posant en evidència que tenen **molt de temps per fer receptes, netejar i cuidar-se.** Tu no fas una recepta de pasta amb ànec, a la qual dediques quatre hores per fer tots els ingredients, si has de treballar per poder sobreviure ja que el sou de la teva parella se'n va directe a pagar el lloguer. Com diu l'amiga [@jelenwalker](https://twitter.com/jelenwalker)són noies que fan coses que pot fer tothom de forma innecessàriament llarga i elaborada.

## Una decisió política

Tanmateix, hi ha una sèrie de components polítics que cal tenir en compte a l'hora de valorar aquest fenòmen. D'una banda, el missatge que transmeten és que han triat aquest estil de vida i la seva conseqüent **pèrdua de drets**, tal i com diu la @xalzfm en aquest [post d'instagram,](https://www.instagram.com/reel/C-va4dqqV2ChuaFWuOB_rIXnNK9mQLUpsgBoO80/?igsh=MWV2NG9wOXE2ZGhzMQ==) i ho han vestit com una alternativa als valors feministes d'igualtat en l'àmbit laboral, igualtat d'oportunitats i independència econòmica que veuen com contraris a les tasques de cures. És a dir, les *tradwives* han aconseguit **simplificar el feminisme per poder-lo atacar de forma més efectiva**, com un nou front en aquesta guerra *anti-woke*, ignorant de forma deliberada la lluita feminista per dignificar les tasques de cures i fer-les compatibles amb el món laboral.

I pensem-hi: aquestes idees casen molt bé amb d'altres que promouen altres influenciadors (individualisme econòmic, llibertat individual, mínim control governamental) i que, casualment, estan en l'agenda de moviments reaccionaris d'ultradreta com els de Trump, Bolsonaro, Abascal o Orriols. Per tant, abraçar el concepte de les *tradwives* va més enllà d'una decisió personal, et fa partícep i còmplice d'una elaborada estratègia de perduts de drets i llibertats de la qual en sortim perjudicats tothom.

## No cal que tot torni

Em definitiva, si bé és cert que diuen que tot acaba tornant, com els texans de cintura ampla o jugar amb baldufes, també és cert que hi ha certes ideologies o maneres de pensar que ja hauríem de tenir superades, si realment aspirem a ser una societat que miri per tothom i només per aquells que es poden permetre el luxe de tenir diners a galerades, segurament a base d'explotar a aquells que en tenen els justos per sobreviure.