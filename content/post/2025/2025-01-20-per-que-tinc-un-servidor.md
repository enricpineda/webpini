---
title: "Per què tinc un servidor propi?"
date: 2025-01-20T00:00:00+02:00
draft: false
---

![Un dit humà assenyalant un núvol digital, com una referècia a les dades que tothom té al núvol](/sobirania.webp)

Per molt que sigui dels que creu que això de "any nou, vida nova" no es pot aplicar de forma general, sí que hi ha cops que un pren decisions a començaments d'any que, com a mínim, tenen la intenció d'esdevenir un canvi en la manera de fer o actuar d'un mateix. I puc dir que aquest inici d'any m'ha portat la intenció de canviar la manera com personalment decideixo tractar les dades digitals que genero en el meu dia a dia, i que durant molts anys han anat conformant el que hom podria anomenar el meu "**jo digital**" una còpia de mi mateix en zeros i uns que s'ha convertit en mercaderia amunt i avall per internet. I d'aquest mercadeig, malauradament, ens en salvem molt pocs, ja que tots hem estat trastejant amb aplicacions com Instagram, Twitter i similars i, per tant, hem anat conformant aquest jo digital que s'ha acabat venent arreu del món.

Per lluitar contra això un ha de tenir molt clar un concepte que és clau: **sobirania tecnològica**. En un món on les nostres sobiranies cada cop estan sent més qüestionades, i on estem acostumats a rebel·lar-nos contra la pèrdua de sobirania fiscal, financera, energètica o nacional (bé, aquesta darrera potser ja no tant), caldria tenir present que la sobirania tecnològica, és a dir, el poder que tenim per decidir qui fa què amb les nostres dades a través d'internet, també és un dret que és fonamental.

I per això jo ara a casa hi tinc un servidor. Potser gestionat una mica "a la pata la llana", i segurament molt millorable en molts aspectes, però és un servidor que em permet tenir:

* El meu propi servidor de GoToSocial per tenir la meva pròpia identitat al Fedivers.
* El meu propi lector / magatzem de marcapàgines digital per guardar-hi les lectures que jo considero importants.
* El meu propi repositori dse fotos, guardat de les mirades descarades de les cada cop més omnipresents IA d'imatges.
* El meu propi gestor de newsletters.

Tots aquests serveis fan que, de cop i volta, ja tingui enmagatzemats a casa i sota el meu control un munt de dades, i per tant pugui dir que estic més a prop d'assolir la sobirania tecnològica. No us negaré que no és un procés fàcil, i que en el camí s'han quedat idees i projectes que he acabat no fent, però el que tinc ara funciona de perles, i **penso compartir-ho amb tothom** amb una sèrie d'articles al blog explicxant, precisament, tot això. Com ho he fet i jo i com algú amb pocs coneixements de manteniment de servidors, també ho pot aconseguir.

Així doncs, ens anem veient per aquest blog al llarg de les properes setmanes!