---
title: "Nova etapa a l'Aixeta"
date: 2023-09-05T09:00:00+02:00
draft: true
---
![Logotip de l'Aixeta](/blog-fedivers-institucions.jpg)

Comencem pel començament: en aquest país, dedicar-se a la cultura és complicat. De fet, en aquest país, **fer qualsevol cosa és complicat**, sobretot si ho vols fer fora del que seria els canals "oficials". Pots fer música, sí, i voler-t'hi dedicar, però si decideixes agafar el camí que no passa per les grans discogràfiques, pel capitalisme salvatge, o pels grans mitjans de comunicació, sobreviure en aquest món és complicat. Has de picar molta pedra, dedicar-hi moltes hores, i de vegades el fruit que en reculls no seria l'esperat.

Tanmateix, amb l'arribada d'allò que fa anys es deia les "noves tecnologies", cada cop més hi ha eines que permeten als *outsiders* poder-se fer un lloc en el panorama cultural i fer arribar les seves propostes a través de la xarxa. Ja sigui amb mitjans convencionals (i subjectes a les normes del capitalisme salvatge) com les xarxes socials més conegudes o els portals de música i vídeo que tothom coneix, o bé amb eines de codi lliure, autogestionades i més justes, però igualment vàlides per difondre la feina feta. I en aquest sentit, les plataformes de mecenatge són un element molt important, ja que permeten aconseguir els mitjans necessaris (normalment monetaris) a canvi de recompenses que engresquen al públic en general.

## Aixeta.cat, una plataforma diferent

Segurament quan es menciona el tema de les plataformes de micromecenatge apareixen exemples com Verkami (que, com a curiositat, té el nom en esperanto). Aquestes plataformes de micromecenatge tenen, com a característica comuna, que els donatius es fan a projectes concrets, i quan s'acaba el temps per aconseguir els diners, no es pot tornar a finançar el projecte. És el model que estem acostumats a veure. Tanmateix, Aixeta.cat funciona d'una manera completament diferent: els donatius són recurrents, en tant que no van a un projecte en concret si nó a un creador o col·lectiu de creadors, de manera que aquests poden aconseguir, de forma fàcil, una manera d'obtenir ingressos periòdicament per dur a terme els seus projectes culturals.

Però clar, un canvi en el model de finançament dels creadors no fa gaire diferent una plataforma d'una altra. En el cas d'Aixeta, el que per mi és el factor que desequilibra la balança al seu favor és que es tracta d'una **entitat sense ànim de lucre**, que fomenta la **llengua i cultura catalanes** i té un compromís ferm amb el programari lliure i les eines tecnològiques per dotar-nos de **sobirania digital**. A més, és un projecte que podríem anomenar de **quilòmetre zero**, amb un nou equip directiu de la terra i compromès amb el país a diversos nivells.

## Malgrat tot, Aixeta necessita la teva ajuda

Un projecte com aquest, sense ànim de lucre i amb aquest nivell alt de compromís amb el país i eñs seus creadors, necessita 
