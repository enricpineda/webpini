---
title: "Mastodon (i el Fedivers) per a institucions públiques"
date: 2023-08-18T09:00:00+02:00
draft: false
---
![Persona manegant un ordinador](/blog-fedivers-institucions.jpg)

Si alguna cosa ha fet bé Elon Musk amb la compra de Twitter i el seu canvi de nom a X, ha estat ajudar a que la gent prengui consciència de l'existència d'alternatives. I quan dic la gent, dic els mitjans de comunicació. Ja comencen a aparèixer articles mencionant Mastodon, i poc a poc aquesta alternativa federada es va fent més popular. Però hi ha tot un sector que es manté resistent a adoptar aquesta nova plataforma, i és el sector públic.

Aquest article pretén ser un **recull dels avantatges i reptes** que jo veig, referents a l'adopció de Mastodon i altres eines del fedivers per part de les institucions públiques com la Generalitat, les Diputacions o fins i tot els ajuntaments, consells comarcals, et al. No pretenc ser gaire exhaustiu, però sí iniciar el debat (a les xarxes socials, és clar) sobre aquest tema.

## Avantatges de l'adopció de Mastodon i altres eines del Fedivers

Un dels primers avantatges que hi veig és la propietat d'allò que s'hi publica. Pensem que les entitats que publiquen contingut, molts cops original, a les xarxes socials com Facebook, Instagram, X i similars, cedeixen part dels drets de publicació, republicació i d'altres a les entitats que allotgen aquests serveis. Això, si ho mirem des del punt de vista d'una entitat pública, hauria de ser motiu per no publicar-hi res. Una xarxa social com Mastodon, en canvi, **permet conservar la propietat d'allò que es publica** en tant que es guarda a dins de l'estructura informàtica de la mateixa institució.

D'altra banda, la creació d'un node institucional permetria agrupar, en un sol lloc, tots els comptes socials de totes les persones, càrrecs, institucions depenents i altres tipus de serveis en un sol lloc, sota les mateixes normes de funcionament. Permet als administradors la gestió de tots aquests comptes de manera ràpida i senzilla, i de cares als usuaris, **permet identificar ràpidament quins són els comptes oficials de les institucions i les persones que hi treballen**, evitant així l'aparició de comptes fraudulents o *fake*.

Per als equips de comunicació de les institucions, la feina de més que caldria fer seria mínima, ja que allò que es publica a les xarxes privatives es pot publicar també a les xarxes federades, pagant només atenció a alguns ajustaments necessaris com la inclusió de text alternatiu a les imatges que es publiquin.

I per últim, i com a més important, a nivell polític i de compromís, **el fedivers s'ajusta perfectament a allò que defensa una institució pública**, com és el servei al ciutadà, la transparència i el compromís amb el programari lliure, revisable públicament i auditable.

## Reptes de l'adopció de Mastodon i altres eines del Fedivers

Un canvi com aquest no està exempts de reptes que cal tenir en compte a l'hora de fer el pas. Un dels més importants és la dedicació de recursos, materials i personals, per tal de poder crear una instància pública al Fedivers. Les institucions haurien d'assumir aquests recursos, i evitar en la mesura del possible l'externalització d'aquests. És molt important que **la propietat d'allò que es publica es quedi a dins de la institució**, és a dir, que l'enmagatzematge estigui gestionat per aquest ens.

D'altra banda, cal facilitar eines als equips de comunicació de les institucions per gestionar no només els comptes institucionals, sinó també les pròpies instàncies: **cal redactar bones polítiques d'ús**, estar pendents dels possibles conflictes que puguin sorgir, i dur a terme totes les tasques de moderació que calgui fer. Ja hem vist que a l'hora de publicar no hi hauria cap mena de problema, però és un repte moderar les interaccions i gestionar informes d'abús, per posar un exemple.

## El Fedivers és un món ple de possibilitats

Poc a poc, les eines relacionades amb el concepte de Fedivers (Mastodon, PixelFed, Pleroma, Misskey, Peertube, etc) van evolucionant i s'assemblen més als productes privatius que ja fa anys que funcionen, aportant una nova capa ètica a la publicació en xarxes socials. L'adopció d'aquestes eines per part de les institucions públiques ja comença a ser un fet en països com Alemanya o el Regne Unit, i s'hauria de començar a plantejar al nostre país. Hi ha potencial perquè institucions com la Generalitat, les diverses diputacions o Televisió de Catalunya aprofitin per començar a investigar aquestes eines,i ofereixn a la ciutadania nous canals justos, lliures i sobirans tecnològicament per contactar i mantenir-se informat.