---
title: "Queixar-se en temps del patriarcat"
date: 2023-10-26T04:00:00+02:00
draft: true
---
![Primer pla de la maneta d'un bebè acabat de néixer](/maneta-bebe.jpg)

Començarem pel començament: **les tasques de case són feixugues**. No conec a ningú que gaudeixi de fer tasques domèstiques com posar rentadores, fregar el terra o plegar roba (ja no parlo de planxar roba perquè cada cop conec menys gent que la planxi). Són tasques pesades, poc agraïdes, i que només comporten la satisfacció de veure-les fetes. I per tant hauria de ser lògic poder-se'n queixar amargament amb les amistats, la parella o la fammília.

Tanmateix, si ets un home, criat i educat com a home, i ja tens una certa edat, t'hauràs trobat amb la disjuntiva de que realment no et pots queixar d'aquestes coses. Si com jo ja t'apropes a la cinquantena i et trobes a tu mateix dient a un grup de persones, entre les quals hi ha dones de la teva edat, com són de feixugues aquestes tasques domèstiques, el comentari més extès que et trobaràs és el de "la meva mare, o la teva, porten tota la vida fent-ho i no s'han queixat mai". Això quan no són elles mateixes, les noies amb qui parles, les que et diuen que "tampoc n'hi ha per tant".

El lector podria arribar a la conclusió de que el fet que els homes no ens puguem queixar d'aquest tema 