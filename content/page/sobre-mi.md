---
title: "Sobre mi"
draft: false
---

![Imatge de portada](/portada_web.jpg)

Reconec que si hi ha alguna cosa complicada en aquest món és parlar d’un mateix sense semblar un pedant, un interessat o un imbècil. I com que no hi ha cosa que estigui més lluny de la realitat que això, serà millor que em descrigui una mica i així tots sortim de dubtes.

Si m’hagués de definir d’alguna manera, potser seria **curiós**. Sóc curiós de mena, i aquesta curiositat m’ha dut a interessar-me per moltes coses al llarg de la meva vida encara que no aconsegueixi aprofundir en res en concret ni tingui la certesa de tenir cap expertesa determinada. Sóc una mica un tastaolletes de coneixement, algú que va decidir ser bibliotecari per ajudar a la gent a ordenar el coneixement i a trobar allò que busca.

A banda d’això, m’agrada la tecnologia, en molts aspectes: des de la part més tècnica (encara que no ho acabi d’entendre) fins a les seves aplicacions en la vida de les persones. Poseu-me a l’abast un nou gadget i gaudiré explorant les seves possibilitats. Doneu-me l’oportunitat de difondre coneixement i continguts a través d’una plataforma, i seré el primer en provar-la. 

També sóc un apassionat de les llengües, tant la seva vessant més lingüística (com es diu vaca en mandarí?) com amb la seva vessant més social (per què l’anglès és la lingua franca de facto al món?). He estudiat llatí i grec clàssics, xinès mandarí i **esperanto**, i m’encanta fer veure que sé alemany tot llegint textos en aquesta llengua, o llençar-me a la piscina i dir quatre coses en francès en un restaurant per provar-me a mi mateix.

La música m’encanta, i en concret la música més sinfònica (tot i que, com amb tot, sóc força eclèctic). I de la mateixa manera com m’agrada escoltar-la, m’agrada provar de fer-ne. Vaig aprendre a tocar el clarinet i la flauta dolça, vaig tastar la música MOD i MIDI, i darrerament fins i tot toco l’ukelele.

Segurament hi ha molts més aspectes de la meva vida que mereixen ser ressenyats, i potser d’altres que no. Podeu seguir llegint [el meu blog](/post), seguir-me a [PixelFed](https://pixelfed.social/enricpineda) o a [Mastodon](https://xerrem.xyz/@enricpineda), i continuar coneixent-me poc a poc.
