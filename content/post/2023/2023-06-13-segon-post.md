---
title: "Aquest és el segon post"
date: 2023-06-13T09:24:59+02:00
draft: true
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae ullamcorper purus. Vestibulum euismod egestas ligula. Suspendisse quam ex, ullamcorper nec eleifend tincidunt, tempus id ligula. Maecenas mauris mauris, vehicula vel nunc at, molestie aliquet elit. Nunc eget blandit est. Sed ac tellus suscipit, scelerisque ligula ut, imperdiet odio. Maecenas non commodo eros. Vestibulum tellus mauris, volutpat in iaculis nec, vestibulum cursus orci. Fusce imperdiet vulputate scelerisque. Pellentesque a feugiat nulla, sed malesuada sem. Donec condimentum diam et risus ultricies dictum. Vivamus sed quam tempor, pellentesque diam ut, facilisis dui.

## Això és una altra prova

Nunc et ipsum varius, maximus ante sit amet, convallis ipsum. Integer quis enim aliquet, blandit nibh a, tempor turpis. Aliquam volutpat mi non aliquam tincidunt. Curabitur in eros risus. Maecenas dictum, nulla vitae vestibulum pretium, justo lorem ornare turpis, et suscipit metus mauris eu massa. Etiam ut elementum lacus, nec ultrices diam. Quisque eu fringilla lacus. Nam at orci mauris. In hac habitasse platea dictumst. 

## Tercer titular

Clar aquí podria haver un [enllaç](https://www.google.com) i no passaria abbsolutament res.