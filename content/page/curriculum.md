---
title: "Currículum"
draft: false
---

## Habilitats professionals

**Aplicacions web**

* Àmplia experiència en HTML, iniciat en Markdown.
* Experìencia menor en Javascript, CSS i disseny accessible
* Programació d'aplicacions web en PHP i Perl.
* Gestió de bases de dades en PostgreSQL, MySQL i MariaDB
* Wordpress:
    * Instal·lació i manteniment de llocs web amb Wordpress
    * Creació de plantilles i temes personalitzats
    * Instal·lació de botigues en línia WooCommerce
* Instal·lació i manteniment de servidors de Mastodon

**Disseny gràfic**

* Disseny de gràfics vectorials amb Inkscape. Nocions de disseny de gràfics vectorials amb Adobe Illustrator.
* Retoc fotogràfic amb GIMP i Photoshop
* Maquetació de material audiovisual amb Scribus i Adobe InDesign.
* Creació i publicació de materials per a xarxes socials.

**Àudio i vídeo**

* Edició de vídeo amb KDenlive i Final Cut
* Edició d'àudio amb Audacity.
* Gravació, edició i publicació de podcasts.
* Gravació, edició i publicació de material audiovisual per a xarxes socials.

## Formació

* Diplomat en Biblioteconomia i Documentació per la Universitat de Barcelona

## Llengües

* Català: llengua materna
* Castellà: nivell nadiu
* Anglès: nivell mig - avançat
* Esperanto: nivell mig

## Altres

**Afiliacions**

* Vocal de Comunicació i Xarxes Socials de l'[Associació Catalana d'Esperanto](https://www.esperanto.cat/)
* Membre de [Puntes Rebels](https://www.puntesrebels.com)