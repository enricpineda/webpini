---
title: El petit Ludwig i l'esperanto, un conte per nens sobre la Llengua Internacional
date: 2024-05-07T00:15:00+02:00
draft: false
---

*Feia temps que em rondava pel cap fer un petit relat sobre la invenció de l'Esperanto dirigit a nens, una mica en la línia dels contes sobre personatges importants que hi ha a moltes llibreries. El cas és que al final li he donat forma a una primera versió, que és la que us presento a continuació. Per cert, si algú el vol ilustrar de franc, potser en podem fer un llibre maco, no creieu?*

Fa molt de temps, en una ciutat anomenada Byalistok, hi havia un nen que es deia Ludwig. Vivia amb els seus pares i els seus deu germans en una casa petita del centre de la ciutat.

El pare era un home gros, que caminava molt recte i sempre preguntava pels deures. “Cal tenir els deures fets”, deia cada dia, i és que el seu pare era mestre. De bon matí sempre deia “добрае утра (Dabràie útra)”, i és que el pare parlava rus.

La mare era una dona gran i carinyosa, que sempre explicava històries de la família quan posava a dormir en Ludwig i els seus germans, o quan els donava de menjar. “Recordeu sempre la família!”, deia sempre. I de bon matí saludava amb un “Gut morgn”, perquè la mare parlava ídix.

En Ludwig, quan anava a l’escola, sempre saludava als seus companys i companyes, i a alguns els deia “Dabràie útra”, a d’altres “Gut morgn”, a d’altres “Dzien dobrý (Djen dobre)” i fins i tot deia “Guten morgen” al despistat d’en Klaus. I és que a l’escola es parlava rus, ídix, polonès i una mica d’alemany.

I és clar, amb tantes llengües, hi havia cops que les nenes i els nens no s’entenien i s’enfadaven. Ves a saber què volia dir la Svetlana amb aquella frase en rus que acabava de dir al despistat d’en Klaus! I és clar, en Klaus s’enfadava i plorava, mentre en Pavel mirava de consolar-lo parlant-li en polonès i no se’n sortia.

En Ludwig pensava: “Tant de bo tots ens poguéssim entendre! Segur que ens barallaríem menys!” I se n’anava a dormir amb aquest pensament al cap.

El Ludwig va créixer i va estudiar molt. Va arribar a ser metge oftalmòleg, però sempre tenia al cap la idea d’un idioma perquè tothom s’entengués. Després de molt treballar, i de fer moltes proves, finalment va presentar al món la seva idea: la Internacia Lingvo, o Llengua Internacional.

Poc a poc, la gent va començar a aprendre aquesta llengua i van començar a veure que era fàcil,que podien parlar entre ells i entendre’s, i que era una llengua justa, ja que no era una llengua de cap poble o nació, però era una llengia per a tothom. I així, aquesta llengua que ara es deia Esperanto es va tornar viral.

En Ludwig volia que les guerres i els conflictes s’acabessin gràcies al seu invent, però no va ser així. Malauradament, les guerres i els conflictes van continuar existint, però aquells que van arribar a aprendre la seva llengua van crear una comunitat de gent respectuosa i pacífica, tal i com el petit Ludwig havia somiat tantes vegades. 

I aquesta comunitat encara existeix, des de la Xina fins a Califòrnia, tot esperant noves persones per dir-los, de bon matí, “bonan matenon”.
