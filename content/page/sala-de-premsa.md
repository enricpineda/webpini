---
title: "Sala de premsa"
draft: false
---

Per bé o per mal, he anat acumulant aparicions a la premsa. Aquesta pàgina em serveix com a arxiu dels llocs on he sortit.

* **[Tertúlia sobre “Mantinc el català”](https://www.ivoox.com/programa-244-mantinc-catala-roger-torne-i-audios-mp3_rf_104399004_1.html)** | Ràdio Vila - 10/03/2023
* **[L’esperanto no és una llengua morta, ha perviscut gràcies a Internet](https://radiosabadell.fm/alacarta/audio/enric-pineda-esperantista-lesperanto-no-es-una-llengua-morta-ha-perviscut-gracies)** |  Ràdio Sabadell – 15/12/2022
* **[La primera pedra – Treu la llengua – L’iniciativa ‘Mantinc el català’](https://www.rac1.cat/a-la-carta/detail/3aec708f-1da7-4abc-abbe-af2000ad1571?program=la-primera-pedra&section=treu-la-llengua)** |  RAC1 – 02/10/2022
* **[Entrevista a Enric Pineda, col·laborador i portaveu de “Mantinc el Català”](https://www.youtube.com/watch?v=5MTUg_4L2rw&t=3s)** |  RAB Ràdio – 20/09/2022
* **[Vortludo furoras ankaŭ en Esperanto](https://www.liberafolio.org/2022/02/01/vortludo-furoras-ankau-en-esperanto/)** |  Libera Folio – 01/02/2022
* **[Enric Pineda i Mabel Rodríguez abandonen Pirates de Catalunya denunciant comportaments masclistes](https://www.diaridesabadell.com/2019/07/03/enric-pineda-mabel-rodriguez-dimiteixen-pirates-de-catalunya/)** |  Diari de Sabadell – 03/07/2019
* **[Enric Pineda (Partit Pirata): ‘Perilla la difusió d’informació i el món de la recerca’](https://www.vilaweb.cat/noticies/enric-pineda-partit-pirata-perilla-la-difusio-de-la-informacio-i-el-mon-de-la-recerca/)** |  Vilaweb – 11/09/2018
* **[Enric Pineda: “Les llibertats d’expressió i d’informació estan bastant vulnerades”](https://www.diaridesabadell.com/2018/06/14/enric-pineda-llibertats-individuals-jutjats-referendum-pirates-per-catalunya/)** |  Diari de Sabadell – 14/06/2018
* **[Enric Pineda, absolt per clonar webs del referèndum, vol denunciar la Policia Nacional](https://radiosabadell.fm/noticia/enric-pineda-absolt-clonar-webs-del-referendum-vol-denunciar-la-policia-nacional)** |  Ràdio Sabadell – 12/06/2018
* **[El coordinador de Pirates, absolt per clonar la web de l’1-O](https://www.isabadell.cat/politica/absolen-el-coordinador-de-pirates-per-clonar-webs/)** |  iSabadell – 12/06/2018
* **[Enric Pineda (Partit Pirata): “Si el poble decideix que la llista més votada no és la de Puigdemont, crec que no hauria de ser president”](https://revistamirall.com/2017/12/17/enric-pineda-partit-pirata/)** |  Revista Mirall – 17/12/2017
* **[Enric Pineda:”L’Estat espanyol ha dut a terme repressió tecnològica”](https://unilateral.cat/2017/11/17/enric-pineda/)** |  L’Unilateral – 17/11/2017