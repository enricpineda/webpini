---
title: "Test"
draft: false
---

<form method="post" action="https://butlleti.enricpineda.cat/subscription/form" class="listmonk-form">
  <div>
    <h3>Subscriu</h3>
    <input type="hidden" name="nonce" />

    <p><input type="email" name="email" required placeholder="Correu electrònic" /></p>
    <p><input type="text" name="name" placeholder="Nom (opcional)" /></p>

    <p>
      <input id="f076d" type="checkbox" name="l" checked value="f076d94d-32e3-416d-a051-c920c648a7b1" />
      <label for="f076d">La BSO de les nostres vides</label>
      <br />
      <span>Un butlletí electrònic setmanal sobre bandes sonores, escrit en català.</span>
    </p>

    <input type="submit" value="Subscriu " />
  </div>
</form>