---
title: "Multiactivista per la llengua (o llengües)"
date: 2024-12-15T00:00:00+02:00
draft: false
---

![Un puny alçat com si fos una reivindicació](/lluita.webp)

Per coses de la vida, i de la meva pròpia evolució com a persona, m'he tornat una mena de multiactivista per la llengua, o en aquest cas meu particular, per les llengües. Un activisme, que, com la gran majoria dels activismes, té com a objectiu **canviar una situació que hom considera injusta**, i així fer d'aquest un món millor. I, a la vegada, un activisme que com la gran majoria dels activismes, és esgotador i sovint s'enfronta a gegants propis del Quixot.

Per una banda, si em segueixes per les xarxes o tens la sort relativa de conèixer-me en persona, sabràs que sóc esperantista. Sóc una d'aquelles persones que considera que **l'adopció de forma massiva de l'esperanto com a llengua internacional ens duria a un món lingüísticament més just**, un món on la comunicació entre els pobles seria més fluïda, i que en alguns casos ens podria portar a una millor resolució dels conflictes que hi ha ara mateix. Però clar, en un món on s'ha adoptat de forma massiva una llengua com l'anglès com a *lingua franca* internacional, proposar que es deixi de banda i que tothom es posi a aprendre una nova llenguia internacional, és una utopia i de les grans. La majoria de gent només es mou per interès, i ja li pots estar presentant al davant una llengua ben fàcil, que trobaran que lluitar per la seva acceptació mundial és, com a mínim, inútil.

I si per si un cas no en tenia pas prou, resulta que també sóc d'aquelles persones que creuen que la meva llengua materna, el català, en tant que llengua d'aquest món nostre, es mereix la mateixas oportunitat que qualsevol altra de florir i de ser usada en tots els espais possibles. Per això **també sóc un defensor d'iniciatives com [Mantinc el Català](https://mantincelcatala.cat/)**, perquè crec que la meva llengua materna ja té prou problemes en el seu dia a dia com perquè nosaltres, a més, n'hi creem de nous a la mínima de canvi tot canviant al castellà quan algú, a casa nostra, s'adreça a nosaltres en aquesta llengua. Han estat molts anys, potser masses i tots, de subordinació lingüística, i en una societat com la nostra, on l'acomodament passa per fer el canvi de llengua amb algú que creu que pel fet d'entendre'l ja se li ha de donar tot mastegat, jo em mantinc tossut i reivindico el meu idioma, ara i adés, i sempre que calgui.

Ja sé que, com a maneres de veure la vida, aquests dos activisdmes lingüístics es poden confondre fàcil amb els deliris d'una persona que ja ha perdut el nord o que refusa seguir amb el ritme dels temps. I potser tindrien raó, però **jo no penso baixar-me del burro**. Aquestes dues idees, aquestes dues lluites, estan modelant el meu present i formen part de la meva persona. Claudicar, deixar-me anar i seguir a la resta significaria deixar una part important del que sóc. I ja he hagut de canviar la meva manera de ser per acomodar-me a la societat, com perquè ara no se'm permeti un xic de personalitat pròpia.

Així doncs, hi ha lluita per estona. I que per molts anys!