---
title: "Ens hem de repensar els parcs infantils urbans"
date: 2024-09-22T00:00:00+02:00
draft: false
---
![Gronxador en un parc](/gronxador.webp)

Ara que m'he plantejat d'escriure un article setmanal al blog em trobo en una situació que espero que tots els opinòlegs del món mundial també hagin experimentat: trobar-se sense idees davant de l'ordinador a punt d'escriure alguna cosa. I la sensació és incòmoda, fins i tot tirant a angoixant. Un mira de donar-li voltes, mirant de veure possibles temes entre l'actualitat de la setmana i res, tot és un desert imaginatiu que faria les delícies del Lawrence d'Aràbia.

Afortunadament per mi, estic passant uns dies de descans a Tordera (Alt Maresme) i he tingut l'oportunitat de fer un parell de voltes en cotxe pel municipi. I m'he quedat gratament sorprès de veure com les criatures torderenques poden jugar en uns parcs que, al pic de l'estiu, estan protegits per unes frondoses copes d'arbres que proporcionen ombra perquè els infants puguin jugar. I això, que és una cosa que tothom estarà d'acord en què és necessari, sembla no estar a l'agenda dels planificadors d'espais infantils a la majoria de poblacions de Catalunya.

De la mateixa manera que és necessari que aquests espais de jocs tinguin elements ludics adaptats a persones amb diversitat motriu, caldria fer per manera que els espais de joc destinats a les criatures **disposessin de zones amb ombres per permetre que els nens i els seus acompayants puguin descansar a la fresca**. I és que ja m'ha passat un parell de vegades que, anant a un parc de jocs infantils, m'he trobat que el sol de justícia del mes de juliol ha convertit el tobogan, normalment una de les atraccions més demandades al parc, en una mena de forn solar que et crema tot el que hi posis al sobre. 

Ajuntaments de Catalunya: tant costa posar uns tendals a sobre per permetre que hi hagi ombra als parcs on no hi hagi criatures durant el mes juliol o agost? Aprofitant que alguns d'aquests parcs estan permanentment en obres, no es pot aprofitar ja l'avinentesa i muntar aquests parasols per tenir ombres durant la canícula? Si el problema és econòmic, m'agradaria saber a què es dediquen els impostos que es paguen a les Diputació o a l'ajuntament de torn. Segurament, a fer dels parcs infantils un lloc on ser-hi tot l'any no és pas cap opció. Seria una cosa massa de hippies, segur.

En qualsevol cas, si estàs pensant el mateix de jo, agrupa't i movilitza't. Aquest tipus de coses són les que només es solucionen si la pressió popular actua. Perquè si hi ha diners per arrebossar la ciutat amb llums per Nadal amb l'excusa de les criatures, n'hi ha d'haver diners per la mainada que juga a fora a l'estiu.