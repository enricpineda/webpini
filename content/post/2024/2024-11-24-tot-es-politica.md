---
title: "T'hi posis com t'hi posis, tot és política"
date: 2024-11-24T00:00:00+02:00
draft: false
---

![Dues persones amb màscarés de Guy Fawkes, una d'elles amb la bandera americana, en una manifestació](/politica.webp)

Sempre m'ha fet molta gràcia la gent que, de forma molt convençuda i vehement, assegura que no li interessa la política. I, tot i no compartir el seu argument ni el seu posicionament, els puc arribar a entedre: el joc de la política institucional, amb tots els seus ets i uts i amb tot el circ mediàtic muntat al seu voltant, fa força mandra. I la mandra es converteix en ràbia quan veus que tots els *xiringuitos*, parcel·les i *tejemanejes* que duen a terme els fan gràcies als nostres impostos. Sí, una d'aquelles coses que es va descuidar de mencionar el darrer anunci de l'Àgència Tributària va ser aquesta: cada partit polític amb un mínim de representació institucional fora dels ajuntaments cobra diners a través de les persones del seu partit que ocupen aquests càrrecs governamentals.

Tanmateix, totes aquestes persones potser no són conscients d'una cosa que a mi m'ha vingut al cap després d'anys en diversos fronts, ja sigui en partits polítics organitzats com a tal com a través d'entitats de tota mena. La política, mal que ens pesi, **ens atravessa cada dia** i condiciona les nostres vides a cada moment. Hi ha una cita trillada d'en Joan Fuster que diu el següent:

> Tota política que no fem nosaltres serà feta contra nosaltres

I té tota la raó. Molta gent, considerada abstencionista pels criteris que creguin més convenients, assegura que no participar en unes eleccions no condiciona perquè el vot d'una persona no pot decidir res, però potser el que no pensen és en l'acte polític que fan tot abstenint-se: la nostra abstenció pavimenta el camí cap a la victòria dels partits del *establishment*, i es pot considerar un acte polític al seu favor en tota regla.

D'altra banda, tot allò que fem com a col·laboradors, voluntaris o ajudants en tasques que tenen una repercussió pública, tamnbé és una forma de fer política. Ser voluntari durant El Gran Recapta és un acte polític, participar de l'associació de veïns també ho és. Fins i tot estar implicat en un equip esportiu de mainada és, de totes totes un acte polític. Perquè en tota aquesta mena d'actes volem fer un canvi en la nostra societat i que aquesta surti millor després de la nostra participació. I pobres de nosaltres si no féssim aquesta sèrie de tasques polítiques i li deixéssim el monopoli de la gestió pública de base als de sempre. Acabaríem francament malament.

Per això, si estàs llegint aquestes línies i ets membre d'una associació, organització, club esportiu o entitat, o simplement ets una mare o pare conscienciat amb l'educació de les teves criatures, o una persona amb ecoansietat que pren la decisió de canviar-se de companyia elèctrica a una que li asseguri un 100% d'energies renovables, deixa'm que et feliciti. Els teus actes polítics són tan o més importants que els que s'anuncien des del faristol del Parlament, i per mi tenen el triple de reconeixement.

Així doncs, si estàs en aquesta tesitura, continúa així!