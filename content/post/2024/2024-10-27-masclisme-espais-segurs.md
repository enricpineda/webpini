---
title: "No hi ha espais segurs a l'esquerra?"
date: 2024-10-27T00:00:00+02:00
draft: false
---

![Imatge en blanc i negre de Íñigo Errejón](/errejon.webp)

El *Refranyer de les dones* de Joan Amades, citat convenientment pel web [Paremiologia Catalana Comparada Digital](https://pccd.dites.cat/), apareix la frase "La dona del Cèsar a més d'ésser honrada ho ha de semblar", que pel que es veu és una mala interpretació d'una frase que va dir Juli Cèsar i que va recollir Plutarc a les seves *Vides paral·leles*. En qualsevol cas, i per no donar encara més voltes, és una frase que es fa servir per dir que, molts cops, les nostres accions han d'anar de la mà de les nostres creences. I en aquest sentit, el fet de trobar conductes obertament masclistes, assetjadores o misògines en espais en teoria segurs per a les dones, les persones del col·lectiu LGTBIQ+ i fins i tot neurodivergents em fa ballar el cap.

El darrer cas conegut, el de Íñigo Errejón i les 12 acusacions d'actuacions masclistes mentre era diputat de Más Madrid i Sumar, s'afegeix a d'altres casos en entorns de partits com els Comuns, la CUP, etc. Ja no parlo de casos que es puguin trobar en partits de l'espectre de la dreta, perquè dono per suposat que les actituds masclistes estan normalitzades en aquests espais.

Que en un espai que es defineix com a feminista apareguin actituds d'aquest tipus és el que em fa pensar que **els homes encara no estem preparats per assumir posicions de poder des del feminisme**. Sempre ens acabarà podent les actituds de les masculinitats tòxiques de sempre, com els abusos de poder, i per tant això farà que les organitzacions amb valors feministes dirigides per homes són, a priori, organitzacions no feministes. I no em sap greu fer aquesta afirmació, però la mera existència d'un, dos, cinc o deu casos d'actituds masclistes en aquests àmbits no em fa tenir gaire confiança en la meitat de la humanitat (entre els quals m'hi incloc).

Cal que hi hagi educació feminista des de les edats més primerenques possibles. És molt necessari que els espais de deconstrucció de la masculinitat tòxica es vagin reproduïnt i que s'hi apuntin tots els homes que puguin, creguin o no que els cal. I sobretot, cal que mentre no es pugui assegurar que qualsevol persona que aspira a un càrrec de poder dins d'una organització feminista no tingui assumits una sèrie de valors i actituds molt concrets, aquests espais no puguin ser ocupats per homes. Així de ras i curt.

Jo no sé si la dona del Cèsar era honrada o ho semblava, pel que sembla el seu marit no ho era gens i va acabar passant a la història, però sí que cal que les teves accions s'adiguin amb els teus ideals, sobretot si has de liderar un espai considerat feminista, Josep Lluís.