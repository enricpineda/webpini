function prenuHFK(dato){
    var date2 = new Date(dato);
    //var date2 = new Date("15/05/1977");
    var y = date2.getFullYear();
    var date1 = new Date("01/01/" + y);
    
    if ((0 == y % 4) && (0 != y % 100) || (0 == y % 400)) {
      var traspas = true;
    }
    
    var diff = Math.abs(date2 - date1);
    var diffdies = Math.ceil(diff / (24 * 3600 * 1000));
    var b = 28;
    
    const mesos = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13"];
    
    var c = Math.floor(diffdies / b);
    var d = diffdies % b;
    var f = d % 7;
    
    if (d == 0) {
      d = 28;
      c = c - 1;
    }
    
    //Ajustem a un any de traspàs
    
    if (traspas === true) {
      if (diffdies == 169) {
        var diadetraspas = true;
      } else if (diffdies >= 170) {
        d = d - 1;
        f = f - 1;
      }
    }
    
    if (diffdies >= 365) {
      var diadelany = true;
    }
    
    const setmana = ["dissabte, ", "diumenge, ", "dilluns, ", "dimarts, ", "dimecres, ", "dijous, ", "divendres, "];
    if (d < 10){d="0"+d;}
    var y2 = y + 10000;
    
    
    if (diadetraspas === true) {
      var retorn = y2 + "-DT";
    } else if (diadelany === true) {
      var retorn = y2 + "-DA";
    } else {
      //var retorn = setmana[f] + d + " de " + mesos[c] + " de " + y2;
      var retorn = y2+"-"+mesos[c]+"-"+d+" (HFK)";
    }
    //retorn = retorn+" "+Date();
return retorn;
}

var toggle = function(elem){
  elem.classList.toggle('visible');
}

// Listen for click events
document.addEventListener('click', function (event) {

	// Make sure clicked element is our toggle
	if (!event.target.classList.contains('menumobil')) return;

	// Prevent default link behavior
	event.preventDefault();

	// Get the content
	var content = document.querySelector(event.target.hash);
	if (!content) return;

	// Toggle the content
	toggle(content);

}, false);