---
title: Son bons els atacs a servidors de Mastodon?
date: 2024-04-07T01:00:00
draft: false
---
![Teclat iluminat d'estètica hacker](/teclat-hacker.jpg)

Fa uns dies els administradors de servidors a Mastodon estàvem una mica esvalotats a causa d'uns atacs de *spam* que havienn inundat les nostres instàncies. I és clar, de cop i volta els missatges amb llistats de servidors a bloquejar van començat a córrer com la pólvora i molts de nosaltres, a banda de mantenir les nostres instàncies ben actualitzades, vam haver de fer repàs de com es feia això de blocar dominis, que semblava que mai ho hauríem de fer i ens hi vam haver de posar.

Tanmateix, si hi pensem bé, podem arribar a una conclusió que, per estrafolària, no deixa de tenir el seu sentit: si ets una persona dolenta que crea eines per fer atacs de missatges no sol·licitats, dediques els teus esforços a atacar una xarxa que, suposadament, no fa servir ningú?

## Quan un atac és la millor de les publicitats

Des d'aquest punt de vista, que Mastodon sigui el blanc dels atacs d'aquest ciberdelinqüents pot voler dir que la xarxa, l'ecosistema en sí, **està guanyant protagonisme** i s'està convertint en un actor a tenir en compte en allò que podríem dir l'ecosistema general d'internet. Aquesta mateixa conclusió a la que han arribat els atacants també la podrien haver tingut anunciants, empresaris i demés púrria neocapitalista per intentar clavar les seves grapes neoliberals en els nostres servidors. Malauradament per ells, molt probablement els mateixos administradors ja s'encarregarien de fer-los treure la idea del cap; les nostres instàncies no estan pensades per ser un altre camp de recol·lecció de dades indiscriminat.

## La importància de tenir una bona xarxa d'admins

Una altre de les coses que m'han cridat l'atenció és la col·laboració i camaraderia existent entre administradors d'instàncies a Mastodon. Just al començament dels atacs de *spam* van començar a circular missatges amb les etiquetes #fediadmin i #fediblock amb llistats de servidors que els administradors de les instàncies del Fedivers (ja no només Mastodon, sinó altres projectes com Misskey o Pleroma) podien agafar per fer bloquejos massius i, d'aquesta manera, minimitzar el risc de dispersió dels comptes maliciosos. Aquests missatges fan evident que darrera de la tasca d'administrar un node del Fedivers hi ha implícita la voluntat d'ajudar a la resta d'administradors i, així, ajudar a cuidar més de l'ecosistema que es manté i creix amb l'ajuda de tothom.

I és que és per coses com aquestes que un acaba estimant aquesta xarxa de xarxes socials, i pensa que en un ecosistema privatiu i dominat per un conjunt de corporacions més preocupades per les teves dades que no pas pel teu benestar, aquest tipus d'actuacions no podran ser mai possibles.

Entreu al fedivers, que hi trobareu un bon ambient, sigueu com sigueu!
