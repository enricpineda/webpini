---
title: Un rellotge en català amb BangleJS
date: 2024-04-25T00:00:00+02:00
draft: false
---
![Logotip i fotografia del rellotge BangleJS](/banglejs.webp)
Tenir un dispositiu electrònic que et mostri informació en català pot semblar quelcom fàcil, però no ho és. Estem acostumats a que aparells tipus telèfons mòbils ens ofereixin aquesta possibilitat, però hi ha marques que s'entesten en fer la dicotomia "venc a Espanya, ergo, ho poso en espanyol" que tan mal ens fa als que no tenim la llengua de Cervantes com a llengua materna, i a més volem que la nostra llengua minoritzada deixi d'estar-ho d'una vegada per totes.

Per això qualsevol iniciativa que permeti tenir un dispositiu en català per dotar a la nostra llengua d'una més que merescuda capa de normalitat s'ha de celebrar, ja sigui un telèfon mòbil, un televisor, o el que tenim ara entre mans, un rellotge intel·ligent.

## BangleJS, un rellotge intel·ligent *hackejable*

El [**BangleJS**](https://banglejs.com/) és un rellotge intel·ligent que cau a dins de la categoria de dispositius de maquinari i programari lliure. A la seva web hi pots trobar no només els materials que s'han fet servir, sinó també els plànols per poder-lo muntar tu mateix. Si ets una mica manetes i t'agrada el món de l'electrònica, el Bangle et pot donar hores de diversió.

Però per als que ens espantem amb un simple endoll de paret el Bangle també ens ofereix coses. Es poden programar aplicacions fent servir [**Espruino**](https://www.espruino.com/), un intèrpret de Javascript que permet als programadors escriure aplicacions que llegeixen la informació del rellotge i dels diversos sensors que té i mostrar-los en pantalla de la manera que vulguem. També es poden incloure imatges, encara que cal tenir en conpte la limitada resolució d'aquest aparell.

En definitiva, per poc més de 70€ tenim a la nostra disposició un dispositiu que podem customitzar com vulguem, per exemple, fent-li dir l'hora en català.

## Muntant Rellotge, l'aplicació catalana que et diu l'hora

Si ja tens el teu BangleJS, et recomano que dediquis uns moments a veure com es carreguen, descarreguen i actualitzen les aplicacions d'aquest rellotge. Hauràs de tenir a mà Chrome, ja que de moment és l'únic navegador que permet connectar dispositius via Bluetooth i modificar-ne el contingut. Així doncs, amb el nostre rellotge vinculat a la botiga d'aplicacions de BangleJS, ja només queda pujar l'aplicació.

Buscarem al cercador la paraula "rellotge" [i ja ens sortirà l'aplicació](https://banglejs.com/apps/?q=rellotge). Fent clic a la icona de pujar, aquesta es muntarà al rellotge, i llavors només caldrà anar al menú de configuració del rellotge, i especificar que volem que Rellotge sigui la nostra aplicació per dir l'hora per defecte.

Per tenir l'experiència completa, busqueu també a la botiga d'aplicacions de Bangle JS els idiomes. Veureu que una subfinestra s'obre i us permet escollir un *locale* o fitxer de configuració local. Seleccioneu el que posa "ca-ES" i un cop fet això el rellotge mencionarà els dies de la setmana com toca.

## I a partir d'ara què?

El rellotge encara té una sèrie de mancances evidents, com el fet que no podem encara traduir la interfície d'usuari al català, tot i que les opcions d'internacionalització deixen entreveure que aviat això podria ser una realitat. Tanmateix, el preu i les opcions de configuració i connexió amb d'altres dispositius com telèfons Android i IOS, fan que aquest rellotgi es postuli com una molt bona opció si vols tenir un rellotge altament configurable.

Arribats a aquest punt vull agrair a en [**Jordi Mas**](https://www.softcatala.org/membres/jmas/), programador de SoftCatalà i traductor al català de projectes de programari lliure a Github, la seva ajuda i tota la feina que ha fet per fer que l'aplicació ara mateix tingui una aparença molt professional. Segurament seguirem treballant per fer-la cada cop una mica millor.